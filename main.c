//
// Created by simone on 25/02/19.
//
// read davis anemometer
#include <stdio.h>
#include <stdint.h>
#include <pigpio.h>
#include <time.h>
#include "ringbuf.h"
#define PIN_READ 22
#define PIN_TEST 23
#define WIND_BUFF_SIZE 200

typedef  struct {
	uint32_t n;
	uint32_t prev_tick;
	uint32_t diff_min;
	uint32_t total_time; //overflow if not cleaned after 72 minutes
	ring_buff_t * buffer;
} digital_t;

void ws_isr(int gpio, int level, uint32_t tick, void* buffer){
    ring_add((ring_buff_t*)buffer, tick);
}
void digital_t_init(digital_t* digital, ring_buff_t* buffer){
    digital->total_time = 0;
    digital->diff_min = UINT32_MAX;
    digital->n = 0;
    digital->buffer = buffer;
}
void read_ws_buffer(void* ws_sensor){
    uint32_t curr_tick;
    time_t curtime;
    time(&curtime);
    struct tm * timeinfo = localtime(&curtime);
    char time_str[10];
    strftime(time_str, 10 , "%H:%M:%S", timeinfo);

    while (ring_read(((digital_t*) ws_sensor)->buffer , &curr_tick)){
        printf("%s,%u\n", time_str ,curr_tick);
    }
}


int main(){
    if (gpioInitialise() < 0) return 1;
    gpioSetMode(PIN_READ, PI_INPUT);
    gpioSetPullUpDown(PIN_READ, PI_PUD_UP);
    gpioSetMode(PIN_TEST, PI_OUTPUT);
    gpioPWM(PIN_TEST, 128);
    gpioSetPWMfrequency(PIN_TEST, 10);
    puts("finished init");

    uint32_t ring_data[WIND_BUFF_SIZE];
    ring_buff_t ws_buffer;
    ring_init(&ws_buffer, (uint32_t *)&ring_data, WIND_BUFF_SIZE);

    digital_t ws;
    digital_t_init(&ws, &ws_buffer);


    gpioSetISRFuncEx(PIN_READ, FALLING_EDGE, -1, ws_isr, &ws_buffer);
    gpioSetTimerFuncEx(0, 2000, read_ws_buffer, &ws);

    puts("time, tick\n");

    while (1){


    }

}

