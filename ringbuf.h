//
// Created by simone on 26/02/19.
//

#include <stdint.h>
#include <stdbool.h>

#ifndef ANEMETRO_RINGBUF_H
#define ANEMETRO_RINGBUF_H


typedef  struct {
    volatile uint32_t * data;
    uint32_t size;
    uint32_t head;
    uint32_t tail;
} ring_buff_t;

bool ring_full(ring_buff_t* buffer){
	return (buffer->head + 1) % buffer->size == buffer->tail;
}
bool ring_empty(ring_buff_t* buffer){
	return  buffer->head == buffer->tail;
}

bool ring_add(ring_buff_t* buffer, uint32_t value) {
	unsigned next = (buffer->head + 1) % buffer->size;
	if (next != buffer->tail) {

		buffer->data[buffer->head] = value;
		buffer->head = next;

		return true;
	}
	else {
		return false;
	}
}
bool ring_read(ring_buff_t* buffer, uint32_t* read_value){
	if (!ring_empty(buffer)){

		*read_value = buffer->data[buffer->tail];
		buffer->tail = (buffer->tail + 1) % buffer->size;
		return true;
	}
	else{
		return false;
	}
}

void ring_init(ring_buff_t* buffer, uint32_t* data, unsigned size){
	buffer->data = data;
	buffer->head = 0;
	buffer->tail = 0;
	buffer->size = size;
}

#endif //ANEMETRO_RINGBUF_H
